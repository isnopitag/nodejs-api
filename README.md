# Api pasada en Nodejs y Express para curso de codigofacilto

## Para correr el proyecto es necesario crear un archivo llamado secret.js en la carpeta config con lo siguiente

~~~javascript
module.exports = {
    cloudinary: {
        api_key: '',
        cloud_name:'',
        api_secret: '',
    },
    jwtSecret: ""
}
~~~

los datos de cloudDinary los puedes encontrar registrandote en la pagina.

el jwtSecret: lo generas con hash o cualquier cadena.

Adicionalmente tienes que:
+ Dar npm install
+ Dar npm install nodemon
+ Tener corriendo mongodb


**Nota: MongoDB no se puede instalar en WSL (Windows Subsystem Linux) **
